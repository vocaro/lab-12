#include <string>
#include <iostream>

using namespace std;

class FootballPlayer {

private:
    string name;

public:

    FootballPlayer(const string &newName) {
        name = newName;
    }

    string getName() const {
        return name;
    }

};

class FootballPlayerList {

private:
    FootballPlayer **list;
    int size;
    int maxSize;

public:

    FootballPlayerList() : FootballPlayerList(100) {}

    FootballPlayerList(int newMaxSize) {
        maxSize = newMaxSize;
        list = new FootballPlayer*[maxSize];
        size = 0;
    }

    ~FootballPlayerList() {
        for (int i = 0; i < size; i++) {
            delete list[i];
        }
        delete [] list;
    }

    // FootballPlayerList will take ownership of the pointer and delete it
    // on destruction.
    void add(FootballPlayer *player) {
        if (size == maxSize) {
            return;
        }

        list[size++] = player;
    }

    FootballPlayer *getElement(int index) const {
        if (index >= size) {
            return nullptr;
        }
        return list[index];
    }
    
    int getSize() const {
        return size;
    }

    void replace(int index, FootballPlayer *player) {
        if (index >= size) {
            return;
        }
        
        delete list[index];
        
        list[index] = player;
    }

    void resize(int newMax) {
        FootballPlayer **newList = new FootballPlayer*[newMax];

        // If the new max is less than the current size, then the new size
        // will be the new max. Otherwise the size doesn't change.
        int newSize = newMax < size ? newMax : size;

        // Copy the pointers from the old list to the new one.
        for (int i = 0; i < newSize; i++) {
            newList[i] = list[i];
        }
        
        // Delete the player objects that are past the new maximum, if any.
        for (int i = newSize; i < size; i++) {
            delete list[i];
        }
        
        delete [] list;
        list = newList;
        size = newSize;
        maxSize = newMax;
    }
};

void print(FootballPlayerList &players) {
    for (int i = 0; i < players.getSize(); i++) {
        cout << players.getElement(i)->getName() << endl;
    }
    cout << endl;
}

int main()
{
    FootballPlayerList players;

    players.add(new FootballPlayer("Mahomes"));
    players.add(new FootballPlayer("Kelce"));
    players.add(new FootballPlayer("Pacheco"));

    print(players);

    players.replace(0, new FootballPlayer("Awesome Player"));

    cout << "After replacing..." << endl;
    print(players);
    
    players.resize(2);

    cout << "After resizing..." << endl;
    print(players);
    
    return 0;
}
